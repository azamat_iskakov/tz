## Setup Docker Swarm with Ansible roles.

## Pre-Check

- Three host machines 
  (Through the `Vagrantfile` I'm provisioning three identical Linux machines (Ubuntu 18.04 LTS) on the same subnet. But in your case it could be ip adresses of physical machines)

Hosts file:
```
node1 ansible_host=172.16.10.11
node2 ansible_host=172.16.10.12
node3 ansible_host=172.16.10.13
```  
## Step 1: Deploy Docker swarm

```sh
$ ansible-playbook deploy-swarm.yml

```
deploy-swarm.yml:
```yml
---
- name: Installing docker
  hosts: docker-nodes
  become: yes
  roles:
    - install-modules
    - docker-installation

- name: initialize docker swarm
  hosts: swarm-managers
  become: yes
  roles:
    - dswarm-init

- name: add workers to the swarm
  hosts: swarm-workers
  become: yes
  roles:
    #- docker-swarm-add-worker
    - dswarm-addworker

```
Now, if we enter in the first node using SSH access and type:

```sh
$ vagrant@node1:~$ docker node ls
```
We will see our initialized cluster with one manager and two worker nodes

![][2]

## Step 2: Deploy customized container and NGINX

```sh
$ ansible-playbook dddepl.yml
```

dddepl.yml:
```yml
---
- name: Deploy NGINX to Swarm
  hosts: swarm-managers
  become: yes
  roles:
    #- delcont
    - customc
    - mynginx
```

```sh
$ vagrant@node1:~$ docker ps
```
![][3]

Let’s check the service status:

```sh
$ vagrant@node1:~$ docker service ls
```
![][6]

The output says that we have 4/4 replicas running. Let’s check the individual tasks:

```sh
$ vagrant@node1:~$ docker service ps mynginx
```
![][7]

Now, if we run browser and enter [172.16.10.11:8080](http://172.16.10.11:8080)
We can see web page with a id of container, date and exchange rates

![][4]

If we visit [172.16.10.11](http://172.16.10.11:80) will see "Welcome to NGINX" default started page

![][5]

[2]: ./Images/2.png
[3]: ./Images/3.png
[6]: ./Images/6.png
[7]: ./Images/7.png
[4]: ./Images/4.png
[5]: ./Images/5.png