# Provision a Docker Swarm Cluster with Vagrant and Ansible

Automatically provision a Docker Swarm cluster composed of one master and two workers.

## Preconditions:

* Linux, Windows or macOS host with at least 8GB RAM
* VirtualBox - https://www.virtualbox.org/
* Vagrant - https://www.vagrantup.com/

The IaC topic will be explored with **Vagrant**, an automation layer above a hypervisor - in our case, VirtualBox. Vagrant leverages a declarative configuration file which describes all your software requirements, packages, operating system configuration, users, and more. With this tool, we will be able to provision three Linux nodes, based on a standard template image, with the proper properties in terms of hardware requirement and network configuration.

Once the nodes are provisioned, we will use **Ansible** for their configuration, turning them from plain Linux nodes to Docker hosts, and ultimately members of Docker Swarm cluster. In order to do that, we will install Ansible master on one node, which will act as a configuration master for the others.

## Requirements

Tested with:
- python==3.8.2
- vagrant==2.2.10
- ansible==2.9.6
    - geerlingguy.pip==2.0.0
    - geerlingguy.docker==2.9.0

But there is no reason it should not work with above's other versions.

## Preparing 

```sh
$ python -m venv env
$ . env/bin/activate
$ pip install ansible
$ ansible-galaxy install geerlingguy.pip geerlingguy.docker
```

## To change the Number of nodes

Change the value of `N` in `Vagrantfile`.
```ruby
N = 3

Vagrant.configure("2") do |config|  
  config.vm.box = "ubuntu/bionic64"
  (1..N).each do |node_id|
    config.vm.define "node#{node_id}" do |node|
      node.vm.hostname = "node#{node_id}"
      node.vm.network "private_network", ip: "192.168.50.#{10+node_id}"
      if node_id == N
        node.vm.provision :ansible do |ansible|
          ansible.limit = "all"
          ansible.groups = {
              "managers" => ["node1"],
              "workers" => ["node[2:#{N}]"], # 03, 04...
              "docker:children" => ["managers", "workers"],
              "docker:vars" =>{
                "swarm_init_interface" => "enp0s8"
              }
          }
          ansible.playbook = "swarm.yaml"
        end
      end
    end
  end
end
```
## Run the Vagrant file

```sh 
$ vagrant up
```

## Verify the Swarm Cluster

Log in the first node using Vagrant's built-in SSH access as the _vagrant_ user:

```sh
$ vagrant ssh node1
```

On this node, which acts as a Swarm Manager, all nodes are now shown in the inventory:

```sh
$ vagrant@node1:~$ docker node ls
```
![][1]

## Apply playbok changes
```sh 
$ vagrant provision # working VMs
# or
$ vagrant up --provision # on precreated VMs
```

[1]: ./Images/1.png

